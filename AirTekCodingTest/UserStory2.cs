﻿using AirTekCodingTest.Model;
using AirTekCodingTest.Services;
using AirTekCodingTest.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirTekCodingTest
{
    public class UserStory2
    {
        const int flightCapacity = 20;

        public void PrintFlightOrderDetails()
        {
            var flightDetails = FlightDetailsService.GetFlightDetails(2).ToList();
            int CountOrderToYyz = 0;
            int CountOrderToYyc = 0;
            int CountOrderToYvr = 0;

            var flightOrderDetails = FlightOrderDetailService.GetFlightOrderDetails();

            foreach (var order in flightOrderDetails)
            {
                if (order.Value.destination == AirportCode.Toronto)
                {
                    CountOrderToYyz++;

                    if (CountOrderToYyz > flightCapacity)
                    {
                        CountOrderToYyz = 0;
                        var fld = flightDetails.Where(f => f.ArrivalAirportCode == order.Value.destination).OrderBy(f => f.Number).FirstOrDefault();
                        flightDetails.Remove(fld);
                    }
                }

                else if (order.Value.destination == AirportCode.Calgary)
                {
                    CountOrderToYyc++;

                    if (CountOrderToYyc > flightCapacity)
                    {
                        CountOrderToYyc = 0;
                        var fld = flightDetails.Where(f => f.ArrivalAirportCode == order.Value.destination).OrderBy(f => f.Number).FirstOrDefault();
                        flightDetails.Remove(fld);
                    }
                }

                else if (order.Value.destination == AirportCode.Vancouver)
                {
                    CountOrderToYvr++;

                    if (CountOrderToYvr > flightCapacity)
                    {
                        CountOrderToYvr = 0;
                        var fld = flightDetails.Where(f => f.ArrivalAirportCode == order.Value.destination).OrderBy(f => f.Number).FirstOrDefault();
                        flightDetails.Remove(fld);
                    }
                }

                var flight = flightDetails.Where(f => f.ArrivalAirportCode == order.Value.destination).OrderBy(f => f.Day).FirstOrDefault();
                Console.WriteLine(GetFormattedString(order.Key, flight));
            }
        }

        private static string GetFormattedString(string orderNumber, FlightDetail flight)
        {
            if (flight != null)
                return $"order: {orderNumber}, flightNumber: {flight.Number}, departure: {flight.DeparturelAirportCode}, arrival: {flight.ArrivalAirportCode}, day: {flight.Day}";
            else
                return $"order: {orderNumber}, flightNumber: not scheduled";
        }
    }
}
