﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirTekCodingTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var userCase1 = new UserStory1();
            userCase1.PrintFlightDetails();

            Console.WriteLine(Environment.NewLine);
            Console.WriteLine(Environment.NewLine);

            UserStory2 userStory2 = new UserStory2();
            userStory2.PrintFlightOrderDetails();
            Console.ReadLine();
        }
    }
}
