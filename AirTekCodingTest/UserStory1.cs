﻿using AirTekCodingTest.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirTekCodingTest
{
    public class UserStory1
    {
        public void PrintFlightDetails()
        {
            var flights = FlightDetailsService.GetFlightDetails(2);

            foreach (var flight in flights)
            {
                Console.WriteLine(GetFormattedString(flight.Number, flight.ArrivalAirportCode, flight.DeparturelAirportCode, flight.Day));
            }
        }

        private static string GetFormattedString(int num, string arrival, string departure, int day)
        {
            return $"Flight: {num}, departure: {departure}, arrival: {arrival}, day: {day}";
        }
    }
}
