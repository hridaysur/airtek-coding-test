﻿using AirTekCodingTest.Model;
using AirTekCodingTest.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirTekCodingTest.Services
{
    public class FlightDetailsService
    {
        public static IEnumerable<FlightDetail> GetFlightDetails(int days)
        {
            var flightDetails = new List<FlightDetail>();
            int flightCount = 1;
            for (int day = 1; day <= days; day++)
            {
                flightDetails.Add(new FlightDetail { Day = day, Number = flightCount++, DepartureAirportName = Airports.Montreal, DeparturelAirportCode = AirportCode.Montreal, ArrivalAirportName = Airports.Toronto, ArrivalAirportCode = AirportCode.Toronto });
                flightDetails.Add(new FlightDetail { Day = day, Number = flightCount++, DepartureAirportName = Airports.Montreal, DeparturelAirportCode = AirportCode.Montreal, ArrivalAirportName = Airports.Calgary, ArrivalAirportCode = AirportCode.Calgary });
                flightDetails.Add(new FlightDetail { Day = day, Number = flightCount++, DepartureAirportName = Airports.Montreal, DeparturelAirportCode = AirportCode.Montreal, ArrivalAirportName = Airports.Vancouver, ArrivalAirportCode = AirportCode.Vancouver });
            }

            return flightDetails;
        }
    }
}
