﻿using AirTekCodingTest.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirTekCodingTest.Services
{
    public class FlightOrderDetailService
    {
        public static Dictionary<string, AirportDestination> GetFlightOrderDetails()
        {
            string fileName = "coding-assigment-orders.json";

            string filePath = Path.Combine((AppDomain.CurrentDomain.BaseDirectory), @"..\..\Data\", fileName);

            using (StreamReader reader = new StreamReader(filePath))
            {
                string jsonString = reader.ReadToEnd();

                Dictionary<string, AirportDestination> orderDetails = JsonConvert.DeserializeObject<Dictionary<string, AirportDestination>>(jsonString);

                return orderDetails;
            }
        }
    }
}
