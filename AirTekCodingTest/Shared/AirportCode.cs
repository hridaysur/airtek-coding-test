﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirTekCodingTest.Shared
{
    public static class AirportCode
    {
        public static readonly string Montreal = "YUL";
        public static readonly string Toronto = "YYZ";
        public static readonly string Calgary = "YYC";
        public static readonly string Vancouver = "YVR";
    }
}
