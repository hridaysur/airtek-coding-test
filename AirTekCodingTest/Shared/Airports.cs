﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirTekCodingTest.Shared
{
    public enum Airports
    {
        Montreal,
        Toronto,
        Calgary,
        Vancouver
    }
}
