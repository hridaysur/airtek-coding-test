﻿using AirTekCodingTest.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirTekCodingTest.Model
{
    public class FlightDetail
    {
        public int Number { get; set; }

        public int Day { get; set; }

        public Airports ArrivalAirportName { get; set; }

        public string ArrivalAirportCode { get; set; }

        public Airports DepartureAirportName { get; set; }

        public string DeparturelAirportCode { get; set; }
    }
}
